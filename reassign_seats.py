from datetime import date
import json
from argparse import ArgumentParser
from pathlib import Path
from random import shuffle


def main(args):
    # Read the current JSON file
    with open(args.current_config, 'r') as fp:
        curr_conf = json.load(fp)

    # Get the lists of reassignable seats and employees
    reassignable_seats = [seat for seat in curr_conf if curr_conf[seat]['reassignable']]
    reassignable_employees = [curr_conf[seat]['occupant'] for seat in reassignable_seats]
    reassignable_employees = [employee for employee in reassignable_employees if employee != 'None']

    if len(reassignable_employees) > len(reassignable_seats):
        raise IOError("There is more employees than available seats !")

    # Shuffle and reassign
    shuffle(reassignable_seats)
    new_conf = curr_conf
    print("="*50 + "\n" + "Seat reassignment !\n" + "="*50)
    for seat, employee in zip(reassignable_seats, reassignable_employees):
        print(f'{employee} -> {seat}')
        new_conf[seat]['occupant'] = employee

    # Make the remaining seats vacant
    for seat in reassignable_seats[len(reassignable_employees):]:
        new_conf[seat]['occupant'] = 'None'

    # Write out the new conf file
    today = date.today()
    outpath = Path(args.current_config).parent/f"seat-assignment_{today}.json"
    with open(outpath, 'w') as fp:
        json.dump(new_conf, fp, indent=4)



if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('current_config', help="Path to the json file describing the current seat arrangement.")
    args = parser.parse_args()
    main(args)
