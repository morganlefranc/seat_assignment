# Seat reassignment script

![Map reference](template.png)

## Requirements

- Python 3

## Usage

```text
usage: reassign_seats.py [-h] current_config

positional arguments:
  current_config  Path to the json file describing the current seat
                  arrangement.

optional arguments:
  -h, --help      show this help message and exit
```

## Remarks

- The map is located [here](https://docs.google.com/presentation/d/1q8nupaGcMq8tbAn2mK2S5H3Z2VdmaEL4BCmV9zsdoY4/edit#slide=id.g82691b305d_1_0).

- Some seats are not reassignable based on specific requests. (I gladly accept bribes :) )

- In order to add a new employee to the model, just add him/her in a vacant and reassignable seat on the current JSON file.
